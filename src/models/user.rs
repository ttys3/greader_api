use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub user_id: String,
    pub user_name: String,
    pub user_profile_id: String,
    pub user_email: String,
}
