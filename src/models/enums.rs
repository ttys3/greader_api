use crate::error::{ApiError, ApiErrorKind};
use std::convert::{From, TryFrom};

#[derive(Copy, Clone)]
pub enum StreamType {
    Stream,
    Category,
}

impl From<StreamType> for String {
    fn from(item: StreamType) -> Self {
        match item {
            StreamType::Stream => "s".to_string(),
            StreamType::Category => "t".to_string(),
        }
    }
}

impl TryFrom<String> for StreamType {
    type Error = ApiError;

    fn try_from(item: String) -> Result<Self, ApiError> {
        let parsed = match &item[..] {
            "s" => StreamType::Stream,
            "t" => StreamType::Category,
            _ => return Err(ApiErrorKind::Parse.into()),
        };
        Ok(parsed)
    }
}

#[derive(Copy, Clone)]
pub enum State {
    #[cfg(feature = "freshrss")]
    All,
    Starred,
    Broadcast,
    Read,
    KeptUnread,
    Like,
    SavedWebPages,
}

// user/-/state/com.google/broadcast
// user/-/state/com.google/broadcast-friends-comments
// user/-/state/com.google/broadcast-friends

impl From<State> for String {
    fn from(item: State) -> Self {
        match item {
            #[cfg(feature = "freshrss")]
            State::All => "user/-/state/com.google/reading-list".to_string(),
            State::Starred => "user/-/state/com.google/starred".to_string(),
            State::Broadcast => "user/-/state/com.google/broadcast".to_string(),
            State::Read => "user/-/state/com.google/read".to_string(),
            State::KeptUnread => "user/-/state/com.google/kept-unread".to_string(),
            State::Like => "user/-/state/com.google/like".to_string(),
            State::SavedWebPages => "user/-/state/com.google/saved-web-pages".to_string(),
        }
    }
}

impl TryFrom<String> for State {
    type Error = ApiError;

    fn try_from(item: String) -> Result<Self, ApiError> {
        let parsed = match &item[..] {
            #[cfg(feature = "freshrss")]
            "user/-/state/com.google/reading-list" => State::All,
            "user/-/state/com.google/starred" => State::Starred,
            "user/-/state/com.google/broadcast" => State::Broadcast,
            "user/-/state/com.google/read" => State::Read,
            "user/-/state/com.google/kept-unread" => State::KeptUnread,
            "user/-/state/com.google/like" => State::Like,
            "user/-/state/com.google/saved-web-pages" => State::SavedWebPages,
            _ => return Err(ApiErrorKind::Parse.into()),
        };
        Ok(parsed)
    }
}
